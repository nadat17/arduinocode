// the setup function runs once when you press reset or power the board
void setup() 
{
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(9, OUTPUT);
  Serial.begin(9600);
}

// the loop function runs over and over again forever
int i = 0;

void triplelight(int lightdur)
{
for (i=0; i<3; i=i+1)
{
  digitalWrite(9, HIGH);   // turn the LED on (HIGH is the voltage level)
  int q=analogRead(9);
  Serial.println(i);
  delay(lightdur);                       // wait for a second
  digitalWrite(9, LOW);    // turn the LED off by making the voltage LOW
  delay(50);    // wait for a second
 }
}

void loop() 
{
  triplelight(50);
  triplelight(500);
  triplelight(50);
  delay(3000);
}


