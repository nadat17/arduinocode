void setup()
{               
    Serial.begin(9600);
    pinMode(A1, INPUT);
    pinMode(9, OUTPUT);
}


char n = 0;
int last = 0;
int read_count = 0;
void loop()
{
    int r = analogRead(A1);   //reading A1 input - the sensor 
    float s = (r/1024.0)*5;   //
    read_count++;
    if (s < 3 && read_count == 3) {
        digitalWrite(9, HIGH);     //Led will turn on
        if (last == 0) {
            Serial.print("changed at ");  //point at which sensor is covered and LED is lit 
            Serial.println(millis());
            read_count = 0;
        }
        last = 1;
    } else {
        digitalWrite(9, LOW);       // Led isn't turned on
        last = 0;
    }
    Serial.println(s);
    delay(200);
}

